import Telegraf from "telegraf";

declare module "telegraf" {
  interface Telegraf {
    launch(): void;
  }
}