import { has } from 'lodash';
import { Option, option } from 'ts-option';
import { UserData, UserAuth } from 'semn-lib';

export class UserMgr {
  private activeUsers: { [id: string]: UserData} = {};

  addUser(id: string, params: UserData) {
    if (!has(this.activeUsers, id)) {
      this.activeUsers[id] = params;
    } else {
      this.activeUsers[id] = params;
    }
  }

  getAuthInfo(id: string): Option<UserAuth> {
    return option(this.activeUsers[id]).map(el => el.token);
  }

  getUserInfo(id: string): Option<UserData> {
    return option(this.activeUsers[id]);
  }
}