import Telegraf from "telegraf";
import * as Markup from 'telegraf/markup';
import * as Extra from 'telegraf/extra';
import * as SocksAgent from 'socks5-https-client/lib/Agent';
import * as SocksProxyAgent from 'socks-proxy-agent';
import * as _ from 'lodash';
import { OntoService, DataIRIs, User, CreateThread, CreateNoteDto } from "semn-lib";
import { UserMgr } from "./services/user-mgr";
import { isNil, isEmpty, first, last } from "lodash";
import { option, none, some, Option } from "ts-option";
import { BotUtils } from "./utils/bot-utils";
const config = require('./config/key.json')
const semnCfg = require('./config/semn-cfg.local.json')

const agent = new SocksProxyAgent(`socks://134.209.76.5:8888`);
const bot = new Telegraf(config.token, {
  telegram: { agent: agent }
});

const url = require('url');
const https = require('https');
function testProxy() {
  const test = `https://api.telegram.org/bot763368110:AAHLfFt404SBDEZPjlz4kl-dGOgJMNnqlfM/getMe`;

  var opts = url.parse(test);
  opts.agent = agent;
  
  https.get(opts, function (res) {
    console.log('"response" event!', res.headers);
    res.pipe(process.stdout);
  });
}

testProxy();

const ontoService = new OntoService(
  semnCfg.backend,
  semnCfg.secret
);
const userMgr = new UserMgr();

function getToken(userData: Option<UserData>): UserAuth {
  return userData.map(el => el.token).orNull;
}

bot.use(async (ctx, next: () => any) => {
  const chat = option(ctx.chat);
  const userId = chat.map(el => el.id.toString()).getOrElse('');
  await userMgr.getAuthInfo(userId).match<Promise<void>>({
    some: () => Promise.resolve(),
    none: () => chat.match({
      some: (chat) => {
        const user = User.fromRaw({ 
          id: chat.id.toString(),
          username: `${chat.first_name} ${chat.last_name}`
        });
        return ontoService.loginUserFromTelegram(user).then(userAuth => {
          userAuth.forEach(userAuth => {
            userMgr.addUser(user.id, {
              token: userAuth,
              user: user,
              selectedThread: none
            });
          });
        })
      },
      none: () => {
        return Promise.resolve();
      }
    })
  }).then(async () => {
    if (!isNil(next)) {
      await next();
    }
  })
})

bot.start(BotUtils.wrapCtx(userMgr, (ctx, userData) => {
  userData.forEach(data => {
    console.log('Create user thread')
    ontoService.createThread(getToken(userData),
      new CreateThread({
        name: 'default',
        description: `default thread for ${data.user.username}`
      })
    ).then(() => {
      ctx.reply(`Hi ${data.user.username}, use the`
        + `\n* /threads - for to get all your threads`
        + '\n /selected_thread - for to get current thread'
        + '\n /current_notes - for to get notes in current thread'
        + '\n /get_my_web - get web url'
      );
    });
  });
}));

bot.help((ctx) => ctx.reply('Send me a sticker'))
bot.on('sticker', (ctx) => ctx.reply('👍'))
bot.hears('hi', BotUtils.wrapCtx(userMgr, (ctx, userData) => {
  userData.forEach(data => {
    ctx.reply(`Hey ${data.user.username} !!!`)
  });
}))

const threadRegExp = new RegExp('/thread_([1-9]+)');
bot.hears(threadRegExp, BotUtils.wrapCtx(userMgr, (ctx, userData) => {
  if (userData.isEmpty) { return; }
  option(ctx.message).forEach((msg) => {
    const idx = Number(threadRegExp.exec(msg.text)[1]) - 1;
    ontoService.getThreads(getToken(userData))
      .then(items => items.getOrElse([]))
      .then((threads) => { 
        option(threads[idx]).match({
          some: (thread) => {
            userData.forEach(data => {
              data.selectedThread = some(thread);
            });
            ctx.reply(`Select thread: ${thread.name}`);
          },
          none: () => ctx.reply(`There isn't any thread with this number`)
        });
      });
  });
}))

bot.hears('/selected_thread', BotUtils.wrapCtx(userMgr, (ctx, userData) => {
  userData.flatMap(el => el.selectedThread).match({
    some: (thread) => {
      ctx.reply(`Current thread: ${thread.name}`);
    },
    none: () => {
      ctx.reply(`You have no selected thread`);
    }
  });
}))

bot.hears('/threads', BotUtils.wrapCtx(userMgr, (ctx, userData) => {
  userData.forEach(data => {
    ontoService.getThreads(getToken(userData))
      .then(items => items.getOrElse([]))
      .then((threads) => {
        const threadsNames = threads.map((el, idx) => {
          return `/thread_${idx + 1} - ${el.name}`
        }).join('\n');
        ctx.reply(`Threads: \n ${threadsNames}`);
      })
      .catch(err => {
        console.log(err);
      });
  });
}))

bot.hears('/current_notes', BotUtils.wrapCtx(userMgr, (ctx, userData) => {
  userData.flatMap(el => el.selectedThread).match({
    some: (thread) => {
      ctx.reply(`Current thread: ${thread.name}`);
      ontoService.getNotesInThread(getToken(userData), thread)
        .then(items => items.getOrElse([]))
        .then((items) => {
          items.forEach(note => {
            console.log(note)
            const noteTexts = note.getData(DataIRIs.hasText);
            const classes = note.getNameClasses();
            if (!isEmpty(noteTexts)) {
              const msg = `
                ${classes.map(el => `[${el}]`).join(' : ')}
                \n ${noteTexts.join('\n')}
              `;
              ctx.reply(msg);
            }
          });
        });
    },
    none: () => {
      ctx.reply(`You have no a selected thread`);
    }
  });
}))

bot.hears('/get_my_web', BotUtils.wrapCtx(userMgr, (ctx, userData) => {
  userData.forEach(async data => {
    const loginToken = await ontoService.genLoginToken(data.user, data.token);
    const token = loginToken.map(el => last(el.token.split(' '))).getOrElse('');

    const url = 'http://127.0.0.1:8100'
    ctx.reply(
      "test",
      Markup.inlineKeyboard([
          Markup.urlButton('MyWeb', `${url}/login?access=${token}`)
      ]).extra()
    );
  });
}))

bot.on('text', BotUtils.wrapCtx(userMgr, (ctx, userData) => {
  userData.forEach(data => {
    option(ctx.message).forEach((msg) => {
      console.log("!!! Message: ", msg);
      if (data.selectedThread.isDefined) {
        const thread = data.selectedThread.orNull;
        if (!isNil(msg.text) && !isEmpty(msg.text)) {
          const note = new CreateNoteDto({ text: msg.text});
          ontoService.addNoteToThread(getToken(userData), thread, note).then(() => {
            ctx.reply(`I got! ${thread.name} : ${msg.text}`);
          });
        }
      }
    });
  });
}))

bot.launch()
