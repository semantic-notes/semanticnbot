import { UserData } from 'semn-lib';
import { Context, ContextMessageUpdate } from 'telegraf';
import { Option, option } from 'ts-option';

import { UserMgr } from '../services/user-mgr';

export class BotUtils {
  static getUserId(ctx: Context): Option<string> {
    return option(ctx.chat).map(el => el.id.toString());
  }

  static getUserData(ctx: Context, userMgr: UserMgr): Option<UserData> {
    return BotUtils.getUserId(ctx).map(id => userMgr.getUserInfo(id).orNull);
  }

  static wrapCtx(userMgr: UserMgr, func: (ctx: ContextMessageUpdate, userData: Option<UserData>) => any) {
    return (ctx: ContextMessageUpdate) => {
      return func(ctx, BotUtils.getUserData(ctx, userMgr));
    }
  }
}