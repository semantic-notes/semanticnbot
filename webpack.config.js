'use strict';
const NodemonPlugin = require( 'nodemon-webpack-plugin' )
const CopyWebpackPlugin = require('copy-webpack-plugin')
const nodeExternals = require('webpack-node-externals');

module.exports = {
    entry: './src/index.ts',
    output: {
        filename: 'build/index.js', // <-- Important
        libraryTarget: 'this' // <-- Important
    },
    target: 'node', // <-- Important
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                options: {
                    transpileOnly: true
                }
            }
        ]
    },
    resolve: {
        extensions: [ '.ts', '.tsx', '.js' ]
    },
    externals: [nodeExternals()], // <-- Important,
    plugins: [
        new CopyWebpackPlugin([
            { from: 'src/config', to: 'build/config' }
        ], {}),
        new NodemonPlugin()
    ]
};